﻿using System;

namespace LesPoteaux_Vache
{
    class Program
    {
        static void Main(string[] args)
        {
            int nombrePiquet;
            float[,] coor;
            float aire;
            float gx;
            float gy;
            
            Console.WriteLine("Combien de piquets ?");

            nombrePiquet = int.Parse(Console.ReadLine());

            if(nombrePiquet > 50)
            {
                Console.WriteLine("Raoul ne possède pas assez de piquets ! Ressaisissez");
                nombrePiquet = int.Parse(Console.ReadLine());
            }

            coor = Saisie(nombrePiquet);
            aire = Aire(coor, nombrePiquet);
            gx = GravitéX(coor, nombrePiquet, aire);
            gy = GravitéY(coor, nombrePiquet, aire);
            bool appartient = AppartenancePolygone(coor, nombrePiquet, gx, gy);

            if (aire < 0)
            {
                aire = -aire;
            }

            Console.WriteLine("Aire = {0}", aire);
            Console.WriteLine("Centre de gravité : {0}, {1}", gx, gy);

            if(appartient)
            {
                Console.WriteLine("La vache est dans l'enclos !");
            }
            else
            {
                Console.WriteLine("La vache s'est enfui du pré !");
            }

        }

        /// <summary>
        /// Effectue la saisie des coordonnées des piquets dans le tableau
        /// </summary>
        /// <param name="nbPiquet">Nombre de piquets</param>
        /// <returns>Coordonnées des piquets</returns>
        public static float[,] Saisie(int nbPiquet)
        {
            int compteurPiquet;
            
            float[,] coordonnées = new float[2,nbPiquet];
            

            for (compteurPiquet = 0; compteurPiquet < nbPiquet; compteurPiquet++)
            {
                Console.WriteLine("Saisie du piquet {0}", compteurPiquet);
                coordonnées[0,compteurPiquet] = float.Parse(Console.ReadLine());
                coordonnées[1,compteurPiquet] = float.Parse(Console.ReadLine());
            }

            return coordonnées;
        }

        /// <summary>
        /// Calcule l'aire de l'enclos
        /// </summary>
        /// <param name="coordonnées">Coordonnées des piquets</param>
        /// <param name="nbPiquet">Nombre de piquets</param>
        /// <returns>Aire de l'enclos (signée)</returns>
        public static float Aire(float[,] coordonnées, int nbPiquet)
        {
            float resultat = 0;

            for(int compteurSegment = 0; compteurSegment < nbPiquet; compteurSegment++)
            {
                resultat = resultat + 
                    (coordonnées[0,compteurSegment] * coordonnées[1,(compteurSegment + 1) % nbPiquet])
                    - (coordonnées[0,(compteurSegment + 1) % nbPiquet] * coordonnées[1,compteurSegment]);
            }

            resultat = (float)(resultat * 0.5);
            
            
            return resultat;
        }

        /// <summary>
        /// Calcule la coordonnée X du centre de gravité G
        /// </summary>
        /// <param name="coordonnées">Coordonnées des piquets</param>
        /// <param name="nbPiquet">Nombre de piquets</param>
        /// <param name="aire">Aire de l'enclos (signée)</param>
        /// <returns>Coordonnée X de G</returns>
        public static float GravitéX(float[,] coordonnées, int nbPiquet, float aire)
        {
            float Gx = 0;
            
            for(int compteurSegment=0; compteurSegment < nbPiquet; compteurSegment++)
            {
                Gx = Gx + ( (coordonnées[0,compteurSegment] + coordonnées[0,(compteurSegment + 1) % nbPiquet]) 
                    * ( (coordonnées[0,compteurSegment] * coordonnées[1,(compteurSegment + 1) % nbPiquet]) 
                    - (coordonnées[0,(compteurSegment + 1 ) % nbPiquet] * coordonnées[1,compteurSegment])));
            }

            Gx = Gx * (1 / (6 * aire));
            
            
            return Gx;
        }

        /// <summary>
        /// Calcule la coordonnée Y du centre de gravité G
        /// </summary>
        /// <param name="coordonnées">Coordonnées des piquets</param>
        /// <param name="nbPiquet">Nombre de piquets</param>
        /// <param name="aire">Aire de l'enclo (signée)</param>
        /// <returns>Coordonnée Y de G</returns>
        public static float GravitéY(float[,] coordonnées, int nbPiquet, float aire)
        {
            float Gy = 0;

            for (int compteurSegment = 0; compteurSegment < nbPiquet; compteurSegment++)
            {
                Gy = Gy + ((coordonnées[1,compteurSegment] + coordonnées[1,(compteurSegment + 1) % nbPiquet])
                    * ((coordonnées[0,compteurSegment] * coordonnées[1,(compteurSegment + 1) % nbPiquet])
                    - (coordonnées[0,(compteurSegment + 1) % nbPiquet] * coordonnées[1,compteurSegment])));
            }

            Gy = Gy * (1 / (6 * aire));


            return Gy;
        }

        /// <summary>
        /// Détermine l'appartenance de G au polygone qu'est l'enclos
        /// </summary>
        /// <param name="coordonnées">Coordonnées des piquets</param>
        /// <param name="nbPiquet">Nombre de piquets</param>
        /// <param name="gx">Coordonnée X de G</param>
        /// <param name="gy">Coordonnée Y de G</param>
        /// <returns>Booléen vrai/faux selon intérieur/extérieur de l'enclos</returns>
        public static bool AppartenancePolygone(float[,] coordonnées, int nbPiquet, float gx, float gy)
        {
            float somme = 0;
            float Thetai;

            for(int i=0; i<nbPiquet; i++)
            {
                float[] v1 = Vecteur(coordonnées, i, gx, gy);
                float[] v2 = Vecteur(coordonnées, (i+1) % nbPiquet, gx, gy);

                Thetai = (float)Math.Acos(
                    (ProdScalaire(v1, v2))
                    /
                    (Norme(v1)
                    *
                    Norme(v2))
                    );

                float signe = Determinant(v1, v2);

                if(signe < 0)
                {
                    Thetai = -Thetai;
                }
                somme = somme + Thetai;
            }

            if(somme == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Calcule des coordonnées d'un vecteur entre le centre G et un des piquets
        /// </summary>
        /// <param name="coordonnées">Coordonnées des piquets</param>
        /// <param name="S">Numéro du piquet</param>
        /// <param name="gx">Coordonnée X de G</param>
        /// <param name="gy">Coordonnée Y de G</param>
        /// <returns>Nouveau vecteur possédant ses propres coordonnées</returns>
        public static float[] Vecteur(float[,] coordonnées, int S, float gx, float gy)
        {
            float[] vecteur = new float[2];
            float vecteurX;
            float vecteurY;

            vecteurX = coordonnées[0,S] - gx;
            vecteurY = coordonnées[1,S] - gy;

            vecteur[0] = vecteurX;
            vecteur[1] = vecteurY;

            return vecteur;
        }

        /// <summary>
        /// Calcule le produit scalaire des deux vecteurs fournis
        /// </summary>
        /// <param name="vecteur1">Vecteur 1 avec coordonnées</param>
        /// <param name="vecteur2">Vecteur 2 avec coordonnées</param>
        /// <returns>Résultat du produit scalaire de V1 et V2</returns>
        public static float ProdScalaire(float[] vecteur1, float[] vecteur2)
        {
            float resultat;

            resultat = vecteur1[0] * vecteur2[0] + vecteur1[1] * vecteur2[1];

            return resultat;

        }

        /// <summary>
        /// Calcule la Norme du vecteur V  
        /// </summary>
        /// <param name="V">Vecteur dont on calcule la Norme</param>
        /// <returns>La Norme du vecteur V</returns>
        public static float Norme(float[] V)
        {
            float resultat;

            resultat = (float)Math.Sqrt
                ((V[0]) * (V[0]) + (V[1]) * (V[1]));

            return resultat;
        }

        /// <summary>
        /// Calcule le déterminant des deux vecteurs fournis
        /// </summary>
        /// <param name="vecteur1">Vecteur 1 avec coordonnées</param>
        /// <param name="vecteur2">Vecteur 2 avec coordonnées</param>
        /// <returns>Déterminant de V1 et V2</returns>
        public static float Determinant(float[] vecteur1, float[] vecteur2)
        {
            float resultat;

            resultat = vecteur1[0] * vecteur2[1] - vecteur1[1] * vecteur2[0];

            return resultat;
        }

    }
}
